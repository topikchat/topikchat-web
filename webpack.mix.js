const mix = require('laravel-mix');
require('dotenv').config();

mix.js('resources/js/app.js', 'public/js')


mix.setPublicPath('public')
   .setResourceRoot('/');

if (mix.inProduction()) {
   mix.version();
}

