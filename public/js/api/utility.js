const WEBURL = "http://localhost:8000"; //link untuk aplikasi topikchat web
const APIURL = "https://api-topikchat.nurasi.com"; //link untuk api



async function updateSession(user) {
    try {
        const response = await $.ajax({
            url: WEBURL + "/update-session",
            method: "POST",
            data: {
                user: user,
            },
        });
        //kalau berhasil kode yang di sini dilanjutkan
        console.log("User telah tersimpan");
        return true;
    } catch (error) {
        console.error("Error " + error.toString());
        return null;
    }
}

async function getUser() {
    try {
        const response = await $.ajax({
            url: WEBURL + "/get-user",
            method: "GET",
        });
        const user = response.user;
        console.log("Berhail mendapatkan user", user);
        return user;
    } catch (error) {
        console.error("Error" + error.toString());
        return null;
    }

}

async function deleteSession() {
    try {
        const response = await $.ajax({
            url: WEBURL + "/delete-session",
            method: "DELETE",
        });

        return true;
    } catch (error) {
        console.error("Error " + error.toString());
        return null;
    }
}