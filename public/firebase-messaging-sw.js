

 // Give the service worker access to Firebase Messaging.
 // Note that you can only use Firebase Messaging here. Other Firebase libraries
 // are not available in the service worker.
 importScripts('https://www.gstatic.com/firebasejs/9.0.2/firebase-app-compat.js');
 importScripts('https://www.gstatic.com/firebasejs/9.0.2/firebase-messaging-compat.js');

 // Initialize the Firebase app in the service worker by passing in
 // your app's Firebase config object.
 // https://firebase.google.com/docs/web/setup#config-object
 const firebaseConfig = {
    apiKey: "AIzaSyCrpIh_t34wm4_jlhQM-j2bGTVHNa94k9s",
    authDomain: "prime-ember-381902.firebaseapp.com",
    projectId: "prime-ember-381902",
    storageBucket: "prime-ember-381902.appspot.com",
    messagingSenderId: "401804531746",
    appId: "1:401804531746:web:649a7a63f45d29dc57927b",
    measurementId: "G-W2S200Z0LV"
};

 firebase.initializeApp(firebaseConfig);

 // Retrieve an instance of Firebase Messaging so that it can handle background
 // messages.
 const messaging = firebase.messaging();
 


// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// Keep in mind that FCM will still show notification messages automatically 
// and you should use data messages for custom notifications.
// For more info see: 
// https://firebase.google.com/docs/cloud-messaging/concept-options

const broadcast = new BroadcastChannel('topikchat-message');

messaging.onBackgroundMessage(function(payload) {
    console.log("hellow");
    broadcast.postMessage({ type: 'new-message', });
});