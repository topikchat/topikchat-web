<?php

use App\Http\Controllers\TokenController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BerandaController;
use App\Http\Controllers\TemplateController;
use App\Http\Controllers\TopikController;
use App\Http\Controllers\FCMController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(["web"])->group(function () {
    Route::get('register', [AuthController::class, 'auth']);
    Route::get('login', [AuthController::class, 'login']);
    Route::get("get-user", [TokenController::class, "getUser"]);
    Route::delete("delete-session", [TokenController::class, "deleteSession"]);
    Route::post("update-session", [TokenController::class, "updateSession"]);
    Route::post('subscribe-to-topic', [FCMController::class, 'subscribeToTopic']);


    Route::middleware(["auth-middleware"])->group(function () {
        // Beranda
        Route::get('sidebar', [BerandaController::class, 'sidebar']);
        Route::get('beranda', [BerandaController::class, 'beranda']);
        Route::get('notif', [BerandaController::class, 'notif']);
        Route::get('msg', [BerandaController::class, 'msg']);

        // Topik
        Route::get('tambah-topik', [TopikController::class, 'tambahTopik']);
        Route::get('edit-topik/{id}', [TopikController::class, 'editTopik']);
        Route::get('group-chat/{id}', [TopikController::class, 'groupChat']);

        Route::get('edit-profile', [UserController::class, 'editProfile']);

        // Route::get('editprofile', [UserController::class, 'editProfile']);

        // Template
        Route::get('template', [TemplateController::class, 'template']);
    });

});
