<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Controller;


class FCMController extends Controller
{
    public function subscribeToTopic(Request $request)
    {
        $token = $request->input('token');
        $topic = $request->input('topic');
        $fcmKey = env('FCM_KEY');

        $response = Http::withHeaders([
            'Authorization' => 'key=' . $fcmKey,
        ])->post('https://iid.googleapis.com/iid/v1/' . $token . '/rel/topics/' . $topic);

        if ($response->failed()) {
            return response()->json(['error' => 'Error subscribing to topic'], 500);
        }

        return response()->json(['message' => 'Subscribed to ' . $topic]);
    }
}