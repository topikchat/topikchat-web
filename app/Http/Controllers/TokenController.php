<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TokenController extends Controller
{
    //fungsi untuk menyimpan token di session sesudah login
    public function updateSession(Request $request)
    {
        $user = $request->input("user");
        $request->session()->put("user", $user);

        return response()->json(["message" => "User berhasil disimpan!"]);
    }

    //Untuk mendapatkan user dari session
    public function getUser(Request $request)
    {
        $user = $request->session()->get("user");
        return response()->json(["user" => $user]);
    }

    //Pada saat tombol logout dipencet, hapus token dari session
    public function deleteSession(Request $request)
    {
        $request->session()->remove("user");
        return response()->json(["message" => "User berhasil dihapus!"]);
    }


}
