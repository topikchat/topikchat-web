<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TopikController extends Controller
{
    //
    function tambahTopik()
    {
        return view('tambah_topik');
    }

    function editTopik($id)
    {
        return view('edit_topik', ["id" => $id]);
    }

    function groupChat($id)
    {
        return view('group_chat', ["id" => $id]);
    }
}
