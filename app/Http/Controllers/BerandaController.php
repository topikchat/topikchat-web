<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BerandaController extends Controller
{
    //
    function sidebar()
    {
        return view('sidebar');
    }
    function beranda()
    {
        return view('beranda');
    }
    function notif()
    {
        return view('notif');
    }
    function msg()
    {
        return view('msg');
    }
}