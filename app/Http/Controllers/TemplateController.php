<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
    //
    function template(){
        return view('template');
    }
    function sidebar(){
        return view('sidebar');
    }
}
