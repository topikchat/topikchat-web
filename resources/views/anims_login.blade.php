@extends('auth_template')

@section('title', 'TopikChat - Login')

@section('content')
    <link rel="stylesheet" href="{{ asset('css/anims_login.css') }}">
    <link rel="stylesheet" href="{{ asset('css/log.css') }}">
    <section>
        <div class="form-box">
            <div class="form-value">
                <!-- <img src="img/chat.png" alt="100"  width="100" height="100"> -->
                <div class="login-group">
                    <div class="login-count"><img src="img/chat.png" alt="100" width="100" height="100"></div>
                    <h2>LOGIN</h2>
                </div>

                <div class="inputbox">
                    <ion-icon name="mail-outline"></ion-icon>
                    <input id="emailOrUsername">
                    <label for="">Email atau Username</label>
                </div>
                <div class="inputbox">
                    <ion-icon name="lock-closed-outline"></ion-icon>
                    <input type="password" id="password">
                    <label for="">Password</label>
                </div>
                <!-- <div class="forget">
                 <label for=""><input type="checkbox">Remember Me</label> <a href="#">Forget Password</a>
                </div> -->
                <button id="login-btn">Log In</button>
                <div class="register">
                    <p>Don't have a account</p><a href="register">Register</a>
                </div>
            </div>
        </div>
    </section>
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>

    <script>
        $(document).ready(function() {
            $("#login-btn").click(function() {
                var emailOrUsername = $("#emailOrUsername").val();
                var password = $("#password").val();

                console.log(emailOrUsername);
                console.log(password);

                if (emailOrUsername == "" || password == "") {
                    toastr.error("Email atau username dan password harus diisi!");
                } else {
                    $.ajax({
                        url: APIURL + "/v1/login",
                        method: "POST",
                        data: {
                            email: emailOrUsername,
                            username: emailOrUsername,
                            password: password,
                        },
                        success: async function (response) {
                            //kalau berhasil login, akan masuk kesini
                            toastr.success("Berhasil login!");
                            const statusSimpan = await updateSession(response.data);
                            if (statusSimpan == true) {
                                window.location.href = "/beranda";
                            }
                        },
                        error: function(error) {
                            // bila gagal login, akan masuk ke sini
                            if (error.responseText) {
                                console.log(error.responseText);
                                var errorResponse = JSON.parse(error.responseText);
                                toastr.error(errorResponse.message);
                            }
                        }
                    });
                }
            });
        });
    </script>
@endsection
