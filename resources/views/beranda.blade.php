@extends('template')

@section('title', 'TopikChat - Beranda')

@section('content')
<div class="container" style="padding: 0 8em 8em 8em">
    <div class="d-flex justify-content-center py-5">
        <div class="d-flex mb-3 flex-column">
            <h4 class="mb-3">Cari Topik Percakapan</h4>
            <div class="custom-search">
                <span class="px-2"><i class="fas fa-search"></i></span>
                <input type="text" placeholder="Search..." class="search" id="searchInput">
            </div>
        </div>
    </div>

    <div class="container3 py-5 hidden" id="not-found">
        <div class="d-flex justify-content-center">
            <h3>Topik Tidak Ditemukan</h3>
        </div>
    </div>
    <div class="d-flex justify-content-start mb-3 px-3 hidden" id="rekomendasi">
        <h4>Rekomendasi Topik</h4>
    </div>
    <div class="wrapp">
        <div class="grid-cards" id="groupChatContainer">

        </div>

    </div>
</div>
<div class="icon-container">
    <a href="tambah-topik" class="add-icon">
        <i class="fas fa-plus"></i>
    </a>
</div>

<div id="loading" class="d-flex flex-column justify-content-center align-items-center"
    style="height: 100vh; position: fixed; top: 0; left: 0; right: 0; bottom: 0; z-index: 999; display: none;">
    <i class="fas fa-circle-notch fa-spin fa-2x py-3"></i>
    <h6>Mendapatkan data</h6>
</div>
</div>
<div class="container mt-3">
    @yield('brd')
</div>
<div class="container mt-3">
    @yield('content')
</div>

<script>
    $(document).ready(async function () {
        var user = await getUser();
        var token = user.token;
        const container = $("#groupChatContainer");
        const rekomendasi = $("#rekomendasi");
        const notFound = $("#not-found");
        const loading = $("#loading");

        function filterCards(searchQuery) {
            container.find('.card').each(function () {
                const card = $(this);
                const title = card.find('.title').text().toLowerCase();
                const description = card.find('.description').text().toLowerCase();
                if (title.includes(searchQuery) || description.includes(searchQuery)) {
                    card.show();
                } else {
                    card.hide();
                }
            });
        }

        $('#searchInput').on('input', function () {
            const searchQuery = $(this).val().trim().toLowerCase();
            filterCards(searchQuery);
        });

        function setupAjax() {
            $.ajaxSetup({
                headers: {
                    "Authorization": "Bearer " + token
                },
            });
        }

        //mendapatkan data group chat dari api
        async function getData() {
            $.ajax({
                url: APIURL + "/v1/group-chat",
                method: "GET",
                success: async function (response) {
                    //kesini jika berhasil mendapatkan data
                    console.log(response.data);
                    if (response.data && response.data.length > 0) {
                        response.data.forEach(function (groupChat) {
                            const card = `
                                <div class="card topik-card  buled-card" onclick="window.location.href = '/group-chat/${groupChat.id}'">
                                    <div>
                                        <h1 class="title">${groupChat.group_name}</h1>
                                        <p class="description">${groupChat.description}</p>
                                    </div>
                                <i class="d-flex align-items center">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                     class="bi bi-person-circle" viewBox="0 0 16 16">
                                    <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                                    <path fill-rule="evenodd"
                                        d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                                    </svg>

                                    <p>${groupChat.members.length} Anggota</p>
                                </i>
                                </div>
                            `;
                            container.append(card);
                        });

                        container.removeClass("hidden");
                        loading.addClass("hidden");
                        rekomendasi.removeClass("hidden");
                    } else {
                        notFound.removeClass("hidden");
                        container.addClass("hidden");
                        rekomendasi.addClass("hidden");
                    }

                },
                error: async function (error) {

                    //Jika ada status 401 di response, maka dia harus delete token yg ada di sesi dan redirect ke login
                    if (response.status == 401) {
                        toastr.error("Sesi habis, anda harus login kembali!");
                        await deleteSession();
                        window.location.href = "/login";
                    } else {
                        toastr.error("Gagal mendapatkan data!");
                        loading.addClass("hidden");
                        notFound.removeClass("hidden");
                        container.addClass("hidden");
                        rekomendasi.addClass("hidden");
                    }

                },
                complete: async function (response) {
                    loading.addClass("hidden");
                    response = response.responseJSON;
                    if (response.newToken) {
                        user.token = response.newToken;
                        await updateSession(user);
                        token = response.newToken;
                    }
                }
            });
        }

        setupAjax();
        getData();
    });
</script>
@endsection