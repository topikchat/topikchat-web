@extends("template")


@section("title", "Group Chat")

@section("chat")

<body style="background-color: #ECECEC">
    <script>
        async function getDataRequest() {
            $.ajax({
                url: APIURL + "/v1/group-chat/" + topikId + "/pendings",
                method: "GET",
                success: async function (response) {
                    console.log("DATA REQUEST : ", response.data);
                    // user_id.val(user.user_id);
                    if (response.data && response.data.length > 0) {
                        response.data.forEach(function (groupChat) {
                            const sidebar = `
                            <div class="user-info py-4">
                                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="currentColor"
                                    class="bi bi-person-circle" viewBox="0 0 16 16">
                                    <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                                    <path fill-rule="evenodd"
                                        d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                                </svg>
                                <p> ${groupChat.user.name}  Ingin Bergabung ke Group ini</p>
                                {{-- <p>Haikal Ingin bergabung ke grup ini</p> --}}
                                <img src="{{ asset('img/Vector (3).png')}}" alt="20"  width="30" height="30" onclick=acceptMasuk(${groupChat.id})>
                            </div>
                        
                        `;
                            $("#request-container").append(sidebar);

                        });
                    }

                },
                // Notif Error
                error: async function (error) {
                    var response = error.responseJSON;

                    if (error.responseText) {
                        var errorResponse = JSON.parse(error.responseText);
                        toastr.error(response.message);
                    }

                },
                complete: async function (response) {
                    response = response.responseJSON;
                    if (response.newToken) {
                        user.token = response.newToken;
                        await updateSession(user);
                        token = response.newToken;
                    }
                    $("#loading").addClass("hidden");
                }


            });
        }

        async function acceptMasuk(id) {
            var approvedAt = new Date().toISOString().slice(0, 19).replace("T", " ");
            $.ajax({
                url: APIURL + "/v1/chat-networks/" + id,
                method: "PUT",
                data: {
                    approved_at: approvedAt,
                },
                success: function (response) {
                    $('#request-container').empty();
                    // toastr.success("Berhasil Edit!");
                    // setTimeout(function() {
                    //     window.location.href = "/edit-topik/" + topikId
                    // }, 1000);
                    getDataRequest();
                },
                // Notif Error
                error: async function (error) {
                    console.log(error);
                    var response = error.responseJSON;

                    if (error.responseText) {
                        var errorResponse = JSON.parse(error.responseText);
                        toastr.error(response.message);
                    }

                },
                complete: async function (response) {
                    response = response.responseJSON;
                    if (response.newToken) {
                        user.token = response.newToken;
                        await updateSession(user);
                        token = response.newToken;
                    }
                    loading.addClass("hidden");
                }
            });
        }
    </script>
    <link rel="stylesheet" href="{{ asset('css/chat.css') }}">
    <div class="main-container">
        <div class="content-container">
            <div>
                <h3 id="nama-topik">Topik 1<h3>
                        <p class="desc" id="deskipsi-topik">Deskripsi Disini
                        <p>
            </div>
            <div class="actions">
                <div class="" id="tombol">
                    <div class="custom-btn btn-spacer btn-purple buled hidden" id="btn-request">Request</div>
                    <div class="custom-btn btn-spacer btn-yellow  buled hidden" id="btn-edit">Edit Topik</div>
                    <div class="custom-btn btn-spacer btn-red buled hidden" id="btn-akhiri">Akhiri Topik</div>
                    <div class="custom-btn btn-spacer btn-green buled" id="btn-gabung">Gabung</div>
                    <div class="custom-btn btn-spacer btn-red buled hidden" id="btn-keluar">Keluar Group</div>
                    <div class="custom-btn btn-spacer btn-orange buled hidden" id="btn-permintaan">Permintaan Sedang
                        Diproses</div>
                </div>
                {{-- <div class="" id="klik">
                    <a href="" class="custom-btn btn-spacer btn-green buled" id="btn-gabung">Gabung</a>
                </div> --}}
            </div>
        </div>
        <div class="chat-box-container" id="chat-box">
            <div class="wrapp justify-content-center align-items-center" id="text-info" style="height: 50vh;">
                <p class="hidden" id="chat-box-info">Anda Belum Bergabung</p>
                <p class="hidden" id="chat-box-info2">Belum Ada Chat</p>
            </div>
            {{-- <div class="chat-container">
                <div class="chat">
                    <div class="user-info">
                        <!-- Jika ada avatar, maka tampilkan avatar pake img, jika tidak, tampilkan logo akun -->
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="currentColor"
                            class="bi bi-person-circle" viewBox="0 0 16 16">
                            <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                            <path fill-rule="evenodd"
                                d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                        </svg>
                        <p>Haikal</p>
                    </div>
                    <div class="message">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed quisquam accusamus, velit cumque
                        provident consequatur quidem temporibus ipsa, mollitia modi in exercitationem atque dicta
                        consectetur expedita odio officiis. Laudantium, maxime!
                    </div>
                </div>
            </div>
            <div class="my-chat-container">
                <div class="my-chat">
                    <div class="my-user-info">
                        <!-- Jika ada avatar, maka tampilkan avatar pake img, jika tidak, tampilkan logo akun -->
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="currentColor"
                            class="bi bi-person-circle" viewBox="0 0 16 16">
                            <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                            <path fill-rule="evenodd"
                                d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                        </svg>
                        <p>Anda</p>
                    </div>
                    <div class="my-message">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed quisquam accusamus, velit cumque
                        provident consequatur quidem temporibus ipsa, mollitia modi in exercitationem atque dicta
                        consectetur expedita odio officiis. Laudantium, maxime!
                    </div>
                </div>
            </div>
            <div class="my-chat-container">
                <div class="my-chat">
                    <div class="my-user-info">
                        <!-- Jika ada avatar, maka tampilkan avatar pake img, jika tidak, tampilkan logo akun -->
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="currentColor"
                            class="bi bi-person-circle" viewBox="0 0 16 16">
                            <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                            <path fill-rule="evenodd"
                                d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                        </svg>
                        <p>Anda</p>
                    </div>
                    <div class="my-message">
                        yah dikacangin
                    </div>
                </div>
            </div>
            <div class="chat-container">
                <div class="chat">
                    <div class="user-info">
                        <!-- Jika ada avatar, maka tampilkan avatar pake img, jika tidak, tampilkan logo akun -->
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="currentColor"
                            class="bi bi-person-circle" viewBox="0 0 16 16">
                            <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                            <path fill-rule="evenodd"
                                d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                        </svg>
                        <p>Haikal</p>
                    </div>
                    <div class="message">
                        Maaf tadi ketiduran wkwk
                    </div>
                </div>
            </div> --}}
        </div>
        <div class="type">
            <textarea type="text" placeholder="Ketik sesuatu disini..." class="form-control" id="pesan"></textarea>
            <div class="send" id="btn-kirim">
                <img src="{{ asset('img/send.png')}}" alt="20" width="30" height="30">
            </div>
        </div>
    </div>
    {{-- DAFTAR REQUETS --}}
    <div class="request-container hidden" id="request-sidebar-container">
        <div class="request hidden" id="request-sidebar">
            <div class="container-request">
                <div class="card-request">
                    <div class="kanan">
                        <div id="myrequest"> <img src="{{ asset('img/close.jpg')}}" alt="20" width="30" height="30">
                        </div>
                    </div>
                    <h3>Daftar Request</h3>
                    <div class="d-flex flex-columns justify-content align-items-center" id="request-container"></div>
                    {{-- <div class="user-info py-4">
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="currentColor"
                            class="bi bi-person-circle" viewBox="0 0 16 16">
                            <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                            <path fill-rule="evenodd"
                                d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                        </svg>
                        <p id="nama-request"> Ingin bergabung ke grup ini</p>
                        <img src="img/Vector (3).png" alt="20" width="30" height="30">
                    </div> --}}
                    {{-- <div class="user-info py-4">
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="currentColor"
                            class="bi bi-person-circle" viewBox="0 0 16 16">
                            <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                            <path fill-rule="evenodd"
                                d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                        </svg>
                        <p>Ecaa Ingin bergabung ke grup ini</p>
                        <img src="img/Vector (3).png" alt="20" width="30" height="30">
                    </div>
                    <div class="user-info py-4">
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="currentColor"
                            class="bi bi-person-circle" viewBox="0 0 16 16">
                            <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                            <path fill-rule="evenodd"
                                d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                        </svg>
                        <p>Neng Ingin bergabung ke grup ini</p>
                        <img src="img/Vector (3).png" alt="20" width="30" height="30">
                    </div>
                    <div class="user-info py-4">
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="currentColor"
                            class="bi bi-person-circle" viewBox="0 0 16 16">
                            <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                            <path fill-rule="evenodd"
                                d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                        </svg>
                        <p>Aldi Ingin bergabung ke grup ini</p>
                        <img src="img/Vector (3).png" alt="20" width="30" height="30">
                    </div> --}}

                </div>
            </div>
        </div>
        <div id="loading" class="d-flex justify-content-center align-items-center"
            style="height: 100vh; background: rgba(0,0,0,0.5); position: fixed; top: 0; left: 0; right: 0; bottom: 0; z-index: 999; display: none;">
            <i class="fas fa-spinner fa-spin fa-3x"></i>
        </div>
    </div>
    </div>
</body>
<script type="module">
    import { initializeApp } from "https://www.gstatic.com/firebasejs/9.0.2/firebase-app.js";
    import { getMessaging, getToken, onMessage } from "https://www.gstatic.com/firebasejs/9.0.2/firebase-messaging.js";
    import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.0.2/firebase-analytics.js";
    import firebaseConfig from '../js/firebase-credentials.json' assert {type: 'json'};

    $(document).ready(async function () {
        var user = await getUser();
        var token = user.token;
        const Request = $("#btn-request");
        const AkhiriTopik = $("#btn-akhiri");
        const EditTopik = $("#btn-edit");
        const chatBox = $("#chat-box");
        const chatBoxInfo = $("#chat-box-info");
        const textInfo = $("#text-info");
        const chatBoxInfo2 = $("#chat-box-info2");
        const gabung = $("#btn-gabung");
        const Keluar = $("#btn-keluar");
        const permintaan = $("#btn-permintaan");
        const kirim = $("#btn-kirim");
        const loading = $("#loading");
        const requestContainer = $("#request-container");
        var approvedAt = new Date().toISOString();

        const name = $("#nama-request")
        const namaTopik = $("#nama-topik");
        const deskripsiTopik = $("#deskripsi-topik");
        var topikId = "{{ $id }}";
        var chatnetworkId;
        var group_id = topikId;
        var user_id = user.id;
        var message = $("#pesan").val();





        function setupAjax() {
            $.ajaxSetup({
                headers: {
                    "Authorization": "Bearer " + token
                },
            });
        }

        function sendDummyChat(pesan) {
            var chatContainer = `<div class="my-chat-container" id="0">
                                        <div class="my-chat">
                                            <div class="my-user-info">
                                                <!-- Jika ada avatar, maka tampilkan avatar pake img, jika tidak, tampilkan logo akun -->
                                                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="currentColor"
                                                class="bi bi-person-circle" viewBox="0 0 16 16">
                                                <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                                                <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                                                </svg>
                                                <p>${user.name}</p>
                                      </div>
                                      <div class="my-message">
                                          ${pesan}
                                      </div>
                                    </div>
                                  </div>`;
            chatBox.append(chatContainer)
            scrollBottom();
        }


        function sendChat() {
            var message = $.trim($("#pesan").val());
            sendDummyChat(message);
            $("#pesan").val("");
            $.ajax({
                url: APIURL + "/v1/chats",
                method: "POST",
                data: {
                    group_id: topikId,
                    user_id: user['id'],
                    message: message,
                },
                success: function (response) {
                    if (response.error) {
                        toastr.error(response.message);
                    } else {
                        getChatData(response.joined);
                    }
                },
                error: async function (error) {
                    var response = error.responseJSON;
                    if (error.responseText) {
                        var errorResponse = JSON.parse(error.responseText);
                        toastr.error(response.message);
                    }


                },
                complete: async function (response) {
                    response = response.responseJSON;
                    if (response.newToken) {
                        user.token = response.newToken;
                        await updateSession(user);
                        token = response.newToken;
                    }
                    loading.addClass("hidden");
                }
            });
        }

        $("#pesan").on('keydown', function (e) {
            if (e.key === 'Enter' && !e.shiftKey) {
                e.preventDefault();
                sendChat();
            }
        });


        kirim.click(function () {
            sendChat();
        });

        async function getChatData(joined) {
            $.ajax({
                url: APIURL + "/v1/chats?group_id=" + topikId,
                method: "GET",
                data: {
                    user_id: user['id'],
                },

                success: function (response) {
                    chatBox.empty();
                    scrollBottom();
                    if (response.data.length > 0) {
                        textInfo.addClass("hidden");
                        chatBoxInfo.addClass("hidden");
                        chatBoxInfo2.addClass("hidden");
                        // Jika data chat kosong
                        response.data.forEach(function (chat) {
                            var chatContainer;
                            //jika user id nya sama dengan user id akun yg sudah login
                            if (chat.user_id != user.id) {
                                //chat milik orang lain
                                chatContainer = `<div class="chat-container" id="${chat.id}"><div class="chat">
                                    <div class="user-info">
                                        <!-- Jika ada avatar, maka tampilkan avatar pake img, jika tidak, tampilkan logo akun -->
                                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="currentColor"
                                        class="bi bi-person-circle" viewBox="0 0 16 16">
                                        <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                                        <path fill-rule="evenodd"
                                          d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                                        </svg>
                                            <p>${chat.user.name}</p>
                                      </div>
                                      <div class="message">
                                       ${chat.message}
                                      </div>
                                    </div>
                                  </div>
                                    `;
                            } else {
                                //chat milik saya
                                chatContainer = `
                                    <div class="my-chat-container" id="${chat.id}">
                                        <div class="my-chat">
                                            <div class="my-user-info">
                                                <!-- Jika ada avatar, maka tampilkan avatar pake img, jika tidak, tampilkan logo akun -->
                                                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="currentColor"
                                                class="bi bi-person-circle" viewBox="0 0 16 16">
                                                <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                                                <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                                                </svg>
                                                <p>${chat.user.name}</p>
                                      </div>
                                      <div class="my-message">
                                          ${chat.message}
                                      </div>
                                    </div>
                                  </div>`;
                            }
                            chatBox.append(chatContainer);
                        });
                    } else {
                        chatBoxInfo.removeClass("hidden");
                        chatBoxInfo2.removeClass("hidden");
                    }
                },
                error: async function (error) {
                    var response = error.responseJSON;

                    if (error.responseText) {
                        var errorResponse = JSON.parse(error.responseText);
                        toastr.error(response.message);
                    }

                },
                complete: async function (response) {
                    response = response.responseJSON;
                    if (response.newToken) {
                        user.token = response.newToken;
                        await updateSession(user);
                        token = response.newToken;
                    }
                    loading.addClass("hidden");
                }


            });
        }

        function scrollBottom() {
            $("html, body").animate({ scrollTop: $(document).height() }, 1000);
            chatBox.animate({ scrollTop: $(document).height() * 1000000 }, 1);
        }

        //mendapatkan data group chat dari api
        async function groupChat() {

            $.ajax({
                url: APIURL + "/v1/group-chat/" + topikId,
                method: "GET",
                success: async function (response) {
                    scrollBottom();
                    console.log(response.data);
                    if (response.data) {
                        namaTopik.text(response.data.group_name);
                        deskripsiTopik.text(response.data.description);
                        chatnetworkId = response.data.chatnetwork_id;

                        console.log(user);
                        if (user['id'] == response.data.user_id) {
                            //pemilik grup
                            Request.removeClass("hidden");
                            AkhiriTopik.removeClass("hidden");
                            loading.addClass("hidden");
                            EditTopik.removeClass("hidden");
                            gabung.addClass("hidden");
                            chatBoxInfo.addClass("hidden");
                            chatBoxInfo2.removeClass("hidden");


                        } else {
                            Request.addClass("hidden");
                            AkhiriTopik.addClass("hidden");
                            EditTopik.addClass("hidden");
                            chatBoxInfo.addClass("hidden");
                            chatBoxInfo2.addClass("hidden");
                            // gabung.removeClass("hidden");   
                            if (response.data.is_requesting) {
                                //semua tombolna hilangin kecuali anu menunggu persetujuan
                                permintaan.removeClass("hidden");
                                Keluar.addClass("hidden");
                                gabung.addClass("hidden");
                                chatBoxInfo.removeClass("hidden");
                                chatBoxInfo2.addClass("hidden");

                            }
                            else if (response.data.joined) {
                                //semua tombol hilanging kecuali tombol gabung
                                permintaan.addClass("hidden");
                                gabung.addClass("hidden");
                                Keluar.removeClass("hidden");
                                chatBoxInfo.addClass("hidden");
                                chatBoxInfo2.removeClass("hidden");
                            }
                            else if (!response.data.is_requesting && !response.data.joined) {
                                // Jika is_requesting dan joined bernilai false
                                permintaan.addClass("hidden");
                                Keluar.addClass("hidden");
                                gabung.removeClass("hidden");
                                chatBoxInfo.removeClass("hidden");
                                chatBoxInfo2.addClass("hidden");
                            }

                        }

                        getChatData();

                    }
                },
                error: async function (error) {
                    var response = error.responseJSON;
                    if (error.responseText) {
                        var errorResponse = JSON.parse(error.responseText);
                        toastr.error(response.message);
                    }

                },
                complete: async function (response) {
                    response = response.responseJSON;
                    if (response.newToken) {
                        user.token = response.newToken;
                        await updateSession(user);
                        token = response.newToken;
                    }
                    loading.addClass("hidden");
                },

            });
        }

        async function getDataRequest() {
            $.ajax({
                url: APIURL + "/v1/group-chat/" + topikId + "/pendings",
                method: "GET",
                success: async function (response) {
                    console.log("DATA REQUEST : ", response.data);
                    // user_id.val(user.user_id);
                    if (response.data && response.data.length > 0) {
                        response.data.forEach(function (groupChat) {
                            const sidebar = `
                            <div class="user-info py-4">
                                <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" fill="currentColor"
                                    class="bi bi-person-circle" viewBox="0 0 16 16">
                                    <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                                    <path fill-rule="evenodd"
                                        d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                                </svg>
                                <p> ${groupChat.user.name}  Ingin Bergabung ke Group ini</p>
                                {{-- <p>Haikal Ingin bergabung ke grup ini</p> --}}
                                <img src="{{ asset('img/Vector (3).png')}}" alt="20"  width="30" height="30" onclick=acceptMasuk(${groupChat.id})>
                            </div>
                        
                        `;
                            $("#request-container").append(sidebar);

                        });
                    }

                },
                // Notif Error
                error: async function (error) {
                    var response = error.responseJSON;

                    if (error.responseText) {
                        var errorResponse = JSON.parse(error.responseText);
                        toastr.error(response.message);
                    }

                },
                complete: async function (response) {
                    response = response.responseJSON;
                    if (response.newToken) {
                        user.token = response.newToken;
                        await updateSession(user);
                        token = response.newToken;
                    }
                    $("#loading").addClass("hidden");
                }


            });
        }

        Request.click(function () {
            getDataRequest();
        });



        $("#btn-keluar").click(async function () {
            $.ajax({
                url: APIURL + "/v1/group-chat/" + topikId,
                method: "DELETE",
                data: {
                    user_id: user['id'],
                },
                success: function (response) {
                    setTimeout(function () {
                        window.location.href = "/beranda";
                    }, 1000);
                },
                // Notif Error
                error: async function (error) {
                    var response = error.responseJSON;

                    if (error.responseText) {
                        var errorResponse = JSON.parse(error.responseText);
                        toastr.error(response.message);
                    }

                },
                complete: async function (response) {
                    response = response.responseJSON;
                    if (response.newToken) {
                        user.token = response.newToken;
                        await updateSession(user);
                        token = response.newToken;
                    }
                    loading.addClass("hidden");
                }


            });
        });

        EditTopik.click(async function () {
            $.ajax({
                url: APIURL + "/v1/group-chat/" + topikId,
                method: "PUT",
                data: {
                    user_id: user['id'],
                },
                success: function (response) {
                    // toastr.success("Berhasil Edit!");
                    setTimeout(function () {
                        window.location.href = "/edit-topik/" + topikId
                    }, 1000);
                },
                // Notif Error
                error: async function (error) {
                    var response = error.responseJSON;

                    if (error.responseText) {
                        var errorResponse = JSON.parse(error.responseText);
                        toastr.error(response.message);
                    }

                },
                complete: async function (response) {
                    response = response.responseJSON;
                    if (response.newToken) {
                        user.token = response.newToken;
                        await updateSession(user);
                        token = response.newToken;
                    }
                    loading.addClass("hidden");
                }
            });
        });

        Keluar.click(function () {

        });

        async function keluarGrup() {
            $.ajax({
                url: APIURL + "/v1/chat-networks/" + chatnetworkId,
                method: "DELETE",
                data: {
                    user_id: user['id'],
                },
                success: function (response) {
                    toastr.success("Berhasil Keluar Group!");
                    setTimeout(function () {
                        window.location.href = "/beranda";
                    }, 1000);
                },
                // Notif Error
                error: async function (error) {
                    var response = error.responseJSON;

                    if (error.responseText) {
                        var errorResponse = JSON.parse(error.responseText);
                        toastr.error(response.message);
                    }

                },
                complete: async function (response) {
                    response = response.responseJSON;
                    if (response.newToken) {
                        user.token = response.newToken;
                        await updateSession(user);
                        token = response.newToken;
                    }
                    loading.addClass("hidden");
                }


            });
        }



        gabung.click(async function () {
            $.ajax({
                url: APIURL + "/v1/chat-networks",
                method: "POST",
                data: {
                    group_id: topikId,
                    user_id: user['id'],
                },
                // Jika Sukses maka akan muncul Notif
                success: function (response) {
                    toastr.success("Berhasil Bergabung !");
                    setTimeout(function () {
                        window.location.reload()
                    }, 1000);
                },
                // Notif Error
                error: async function (error) {
                    var response = error.responseJSON;

                    if (error.responseText) {
                        var errorResponse = JSON.parse(error.responseText);
                        toastr.error(response.message);
                    }

                },
                complete: async function (response) {
                    response = response.responseJSON;
                    if (response.newToken) {
                        user.token = response.newToken;
                        await updateSession(user);
                        token = response.newToken;
                    }
                    loading.addClass("hidden");
                }
            });
        });


        setupAjax();
        groupChat();


        $("#btn-request").click(function () {
            $("#request-sidebar-container").toggleClass("hidden");
            $("#request-sidebar").toggleClass("hidden");
        });
        $("#myrequest").click(function () {
            $("#request-sidebar-container").toggleClass("hidden");
            $("#request-sidebar").toggleClass("hidden");
        });

        $("#btn-akhiri").click(function () {
            var apakahKeluar = confirm("Apakah anda yakin?");
            if (apakahKeluar) {
                keluarGrup();
            }
        });
        $("#btn-cancel").click(function () {
            $("#card-sidebar-container").toggleClass("hidden");
            $("#card-sidebar").toggleClass("hidden");
        });

        // const firebaseConfig = {
        //     apiKey: "AIzaSyCrpIh_t34wm4_jlhQM-j2bGTVHNa94k9s",
        //     authDomain: "prime-ember-381902.firebaseapp.com",
        //     projectId: "prime-ember-381902",
        //     storageBucket: "prime-ember-381902.appspot.com",
        //     messagingSenderId: "401804531746",
        //     appId: "1:401804531746:web:649a7a63f45d29dc57927b",
        //     measurementId: "G-W2S200Z0LV"
        // };

        const app = initializeApp(firebaseConfig);
        const analytics = getAnalytics(app);
        const messaging = getMessaging(app);

        console.log("WOY");
        console.log(app, messaging,);

        const broadcast = new BroadcastChannel('topikchat-message');

        console.log(Notification.permission);

        if (Notification.permission === "granted") {
            addServiceWorker();
            initFirebaseMessagingRegistration();

        } else {
            console.log("HERE");
            Notification.requestPermission()
                .then((permission) => {
                    if (permission === "granted") {
                        addServiceWorker();
                        initFirebaseMessagingRegistration();

                    }
                });
        }

        function addServiceWorker() {
            if ("serviceWorker" in navigator) {
                navigator.serviceWorker
                    .register("/firebase-messaging-sw.js")
                    .then((registration) => {
                        broadcast.onmessage = (event) => {
                            if (event.data && event.data.type === 'new-message') {
                                getChatData();
                            }
                        };
                        console.log("Service Worker registered with scope:", registration.scope);
                    })
                    .catch((error) => {
                        console.error("Service Worker registration failed:", error);
                    });

            }
        }




        function subscribeToTopic(token, topic) {
            $.ajax({
                url: '/subscribe-to-topic',
                method: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                    token: token,
                    topic: topic
                }),
                success: function (data) {
                    console.log(data.message);
                },
                error: function (error) {
                    console.error(error);
                }
            });
        }


        function initFirebaseMessagingRegistration() {

            getToken(messaging, { vapidKey: firebaseConfig['vapidKey'] })
                .then((t) => {
                    console.log("token", t);
                    subscribeToTopic(t, 'chat');
                })
                .catch(function (err) {
                    console.log("Didn't get notification permission", err);
                });

            onMessage(messaging, (payload) => {
                getChatData();
            });
        }


    });




</script>


@endsection