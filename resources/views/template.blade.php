<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('plugin/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
    {{--
    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('css/beranda.css') }}">
    <link rel="stylesheet" href="{{ asset('css/styleicon.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sidebar.css') }}">
    <link rel="stylesheet" href="{{ asset('css/notip.css') }}">
    {{--
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css"> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
        integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Roboto&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"
        integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"
        integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        })
    </script>
    <script src="{{ asset('js/api/utility.js') }}"></script>


</head>

<body>
    <!-- Sidebar List Chat Hidden, Munculkan Seperlunya -->
    <div class="sidebar-container hidden" id="message-sidebar-container">
        <div class="sidebar box-shadow sidebar-hidden" id="message-sidebar">
            <div class="kanan1">
                <div id="mymessage"> <img src="{{ asset('img/close.jpg')}}" alt="20" width="30" height="30"></div>
            </div>
            <h5 class="d-flex justify-content-center align-items-center" style="color: #3DB18E">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                    class="bi bi-chat-left-text-fill" viewBox="0 0 16 16">
                    <path
                        d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H4.414a1 1 0 0 0-.707.293L.854 15.146A.5.5 0 0 1 0 14.793V2zm3.5 1a.5.5 0 0 0 0 1h9a.5.5 0 0 0 0-1h-9zm0 2.5a.5.5 0 0 0 0 1h9a.5.5 0 0 0 0-1h-9zm0 2.5a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5z" />
                </svg> Message
            </h5>
            <div class="d-flex flex-column justify-content-center align-items-center" id="messagecontainer">

            </div>
        </div>
    </div>


    <!-- Sidebar User Hidden, Munculkan Seperlunya -->

    <div class="sidebar-container hidden" id="user-sidebar-container">
        <div class="sidebar col-lg-2 box-shadow sidebar-hidden" id="user-sidebar">
            <div class="kanan1">
                <div id="mysidebar"> <img src="{{ asset('img/close.jpg')}}" alt="20" width="30" height="30"></div>
            </div>
            <h3 class="d-flex justify-content-center">PROFIL</h3>
            <a href="http://localhost:8000/edit-profile" @if (request()->route()->uri == 'dashbord') class='active'
                @endif>
                <img src="https://www.logolynx.com/images/logolynx/4b/4beebce89d681837ba2f4105ce43afac.png" alt="20"
                    width="30" height="30">
                Edit Profile
            </a>
            <a @if (request()->route()->uri == 'Logout') class='active' @endif id="logout-btn">
                <img src="https://icon-library.com/images/logout-icon-android/logout-icon-android-29.jpg" alt="20"
                    width="30" height="30">
                Logout
            </a>
        </div>
    </div>

    <nav class="navbar box-shadow">

        <div class="logo-navbar">
            <img src="{{ asset('img/chat.png') }}" alt="50" width="64" height="64">
            <a href="http://localhost:8000/beranda" style="text-decoration: none;">
                <h2 style="color: #3DB18E" s>Topik Chat</h2>
            </a></li>
            {{-- <div class="#"><a href="beranda" style="color: #3DB18E">
                    <h3>TopikChat
                </a></h3>
            </div> --}}
        </div>
        <ul class="nav-links">
            <li id="message-btn">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                    class="bi bi-chat-left-text-fill" viewBox="0 0 16 16">
                    <path
                        d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H4.414a1 1 0 0 0-.707.293L.854 15.146A.5.5 0 0 1 0 14.793V2zm3.5 1a.5.5 0 0 0 0 1h9a.5.5 0 0 0 0-1h-9zm0 2.5a.5.5 0 0 0 0 1h9a.5.5 0 0 0 0-1h-9zm0 2.5a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1h-5z" />
                </svg>
            </li>
            </div>

            </div>
            <li id="user-btn">
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                    class="bi bi-person-circle" viewBox="0 0 16 16">
                    <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                    <path fill-rule="evenodd"
                        d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z" />
                </svg>
            </li>
            </div>
        </ul>
        <div class="burger">
            <div class="line1"></div>
            <div class="line2"></div>
            <div class="line3"></div>
        </div>
    </nav>
    <div class="hidden" id="notif-sidebar-container">
        <div class="notification sidebar-hidden" id="notif-sidebar">
            <div class="topik-card  buled-card box-shadow">
                <div class="kanan-notif">
                    <div id="mynotif"> <img src="{{asset('img/close.jpg')}}" alt="20" width="30" height="30">
                    </div>
                </div>
                <div class="py-2 element">
                    <input type="submit" value="Topik 1 telah Diakhiri" class="form-control text-white selector"
                        style="background-color:#aaadac;">
                </div>
                <div class="py-2">
                    <input type="submit" value="Topik 1 telah Diakhiri" class="form-control text-white selector"
                        style="background-color:#aaadac;">
                </div>
                <div class="py-2">
                    <input type="submit" value="Topik 1 telah Diakhiri" class="form-control text-white selector"
                        style="background-color:#aaadac;">
                </div>
            </div>
            <button class="close-button">&times;</button>
        </div>
    </div>


    <div class="container">
        @yield('content')
    </div>
    <div class="chat-page">
        @yield('chat')
    </div>
</body>




{{-- log out --}}
<script>
    $(document).ready(async function () {
        var user = await getUser();

        async function dataMessage() {
            const container = $("#messagecontainer");
            console.log("dataMessage");
            $.ajax({
                url: APIURL + '/v1/chat-networks?user_id=' + user['id'],
                method: "GET",
                success: async function (response) {
                    //kesini jika berhasil mendapatkan data Message
                    console.log(response.data);
                    if (response.data && response.data.length > 0) {
                        response.data.forEach(function (groupChat) {
                            if (groupChat.group.is_active && groupChat.left_at == null) {
                                const sidebar = `
                         <a  class='d-block' style="display: block;" href='/group-chat/${groupChat.group.id}''>
                                <img src="{{ asset('img/Group 22.png') }}" alt="20"  width="30" height="30"  id="mesagge" >
                                ${groupChat.group.group_name}</a>
                        `;
                                container.append(sidebar);
                            }


                        });
                    }
                },
                error: async function (response) {
                    response = response.responseJSON;
                    //Jika ada status 401 di response, maka dia harus delete token yg ada di sesi dan redirect ke login
                    if (response.status == 401) {
                        toastr.error("Sesi habis, anda harus login kembali!");
                        await deleteSession();
                        window.location.href = "/login";
                    } else {
                        toastr.error("Gagal mendapatkan data!");
                        loading.addClass("hidden");
                        notFound.removeClass("hidden");
                        container.addClass("hidden");
                        rekomendasi.addClass("hidden");
                    }

                },
                complete: async function (response) {
                    response = response.responseJSON;
                    if (response.newToken) {
                        user.token = response.newToken;
                        await updateSession(user);
                        token = response.newToken;
                    }
                }
            });
        }

        $("#message-btn").click(function () {
            $("#message-sidebar-container").toggleClass("hidden");
            $("#message-sidebar").toggleClass("sidebar-hidden");
            dataMessage();
        });


        $('#user-btn').click(function () {
            $("#user-sidebar-container").toggleClass("hidden");
            $("#user-sidebar").toggleClass("sidebar-hidden");

        });
        $('#notif-btn').click(function () {
            $("#notif-sidebar-container").toggleClass("hidden");
            $("#notif-sidebar").toggleClass("sidebar-hidden");
        });
        $("#mysidebar").click(function () {
            $("#user-sidebar-container").toggleClass("hidden");
            $("#user-sidebar").toggleClass("sidebar-hidden");
        });
        $("#mymessage").click(function () {
            $("#message-sidebar-container").toggleClass("hidden");
            $("#message-sidebar").toggleClass("sidebar-hidden");
        });
        $("#mynotif").click(function () {
            $("#notif-sidebar-container").toggleClass("hidden");
            $("#notif-sidebar").toggleClass("sidebar-hidden");
        });

        function setupAjax() {
            $.ajaxSetup({
                headers: {
                    "Authorization": "Bearer " + user['token'],
                },
            });
        }


        $("#logout-btn").click(function () {
            console.log("logout")
            $.ajax({
                url: APIURL + "/v1/logout",
                method: "POST",
                success: async function (response) {
                    console.log(response);
                    const statusDelete = await deleteSession();
                    if (statusDelete == true)
                        window.location.href = "/login";
                },
                error: async function (error) {
                    console.log(error);
                    if (error.responseText) {
                        const statusDelete = await deleteSession(response.data);
                        toastr.error(errorResponse.message);
                        window.location.href = "/login";
                    }
                }
            });


        });

    });
</script>

</html>