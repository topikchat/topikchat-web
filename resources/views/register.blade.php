@extends("auth_template")

@section("title", "TopikChat - Register")

@section("content")
<div class="background">

    <div class="container">
        <div class="d-flex justify-content-center">
            <img src="img/chat.png" alt="100"  width="180" height="180">
         </div>
    <div class="container">
        {{-- <div class="w-50 center px-3 mx-auto"> --}}
            <div class="container h-100">
                <div class="row h-50 d-flex justify-content-center align-items-center">
                      <div class="card">
                         <div class="card-body">
                            <div class="d-flex justify-content-center">
                                <h1 class="form-title">TopikChat</h1>
                            </div>
                            <div class="d-flex justify-content-center">
                                <h3>Register</h3>
                             </div>
                         <div class="buled py-2">
                        <form action="">
                            @csrf
                            <div class="mb-3">
                                <label for="" class="from-label fonts">Nama Lengkap</label>
                                <input type="text" name="nama" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label for="" class="from-label fonts">Username</label>
                                <input type="username" name="username" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label for="" class="from-label fonts">Email</label>
                                <input type="email" name="email" class="form-control">
                            </div>
                            <div class="mb-3">
                                <label for="" class="from fonts">Kata Sandi</label>
                                <input type="password" name="kata sandi" class="form-control">
                            </div>
                            <div class="col-sm-12 py-2">
                                <input type="submit" value="Register" class="form-control text-white buled" style="background-color:#3DB18E;">
                            </div>
                            <div class="mt-3 text-center">
                                <p>Sudah Punya Akun? <a href="/login" style="color:#3DB18E;">Login</a></p>
                            </div>
                        </form>
                    </div>
                 </div>
            </div>
        </div>
    </div>
</div>
@endsection
