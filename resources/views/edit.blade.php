@extends('template')

@section('title', 'TopikChat - Edit Profile')

@section('content')
<link href="{{ asset('css/edit-profile.css') }}" rel="stylesheet">

<div class="container d-flex flex-column justify-content-center" style="height: 85vh">
    <h2 class="text-center mb-5">Edit Profile</h2>
    <div class="wrapp flex-column">
        <div class="card py-3 px-3">
            <div class="kolom mb-3">
                <label for="" class="from-label">Nama Lengkap</label>
                <input type="text" id="nama_lengkap" name="nama_lengkap" class="form-control">
            </div>
            <div class="kolom mb-3">
                <label for="" class="from-label">Username</label>
                <input type="text" id="username" name="username" class="form-control">
            </div>
            <div class="kolom mb-3">
                <label for="" class="from-label">Email</label>
                <input type="email" id="email" name="email" class="form-control">
            </div>
            <div class="kolom">
                <input type="submit" value="Edit" class="form-control text-white selector buled"  id="edit-profile"
                    style="background-color:#3DB18E;">
            </div>
        </div>
    </div>
    <div id="loading" class="d-flex justify-content-center align-items-center" style="height: 100vh; background: rgba(0,0,0,0.5); position: fixed; top: 0; left: 0; right: 0; bottom: 0; z-index: 999; display: none;">
        <i class="fas fa-spinner fa-spin fa-3x"></i>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
<script>
      $(document).ready(async function() {
        var user = await getUser();
        var token = user.token;
        const namaInput = $("#nama_lengkap");
        const username = $("#username");
        const email = $("#email");
        const loading = $("#loading");

        function setupAjax(){
            $.ajaxSetup({
                headers: {
                    "Authorization": "Bearer " + token
                },
            });
        }

        function setInput(){
            namaInput.val(user.name);
            username.val(user.username);
            email.val(user.email);
            $("#loading").addClass("hidden");
          
        }

        $("#edit-profile").click(async function() {
            loading.removeClass("hidden");
            $.ajax({
                url: APIURL + "/v1/edit-profile",
                method: "PUT",
                data: {
                    name: namaInput.val(),
                    username: username.val(),
                    email: email.val(),
                },
                success: async function(response) {
                    toastr.success("Berhasil mengubah data diri");
                    setTimeout(function(){
                        window.location.href = "/beranda";
                    }, 1000);
                },
                error: async function(error) {
                    var response = error.responseJSON;

                    // Jika ada status 401 di response, maka dia harus delete token yg ada di sesi dan redirect ke login
                    if (response.status == 401) {
                        toastr.error("Sesi habis, anda harus login kembali!");
                        await deleteSession();
                        window.location.href = "/login";
                    } else {
                        toastr.error(response.message);
                    }
                },
                complete: async function(response){
                    response = response.responseJSON;
                    console.log(response);
                    if (response.newToken) {
                        user.token = response.newToken;
                        await updateSession(user);
                        token = response.newToken;
                             // UserBaru
                            if (response.data) {
                                userBaru['token'] = data;
                                await updateSession(user);
                                token = response.userBaru;

                            }
                    } 

                    loading.addClass("hidden");
                }
            });
        });

        setupAjax();
        setInput();

      });
</script>
@endsection
