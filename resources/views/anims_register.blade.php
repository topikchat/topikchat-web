@extends('auth_template')

@section('title', 'TopikChat - Register')

@section('content')
    <link rel="stylesheet" href="{{ asset('css/anims_login.css') }}">
    <link rel="stylesheet" href="{{ asset('css/log.css') }}">

    <section>
        <div class="form-box">
            <div class="form-value">
                <!-- <img src="img/chat.png" alt="100"  width="100" height="100"> -->
                <div class="login-group">
                    <div class="login-count"><img src="img/chat.png" alt="100" width="100" height="100">
                    </div>
                    <h2>REGISTER</h2>
                </div>
                <div class="inputbox">
                    <ion-icon name="person-circle-outline"></ion-icon>
                    <input id="name">
                    <label for="">Nama Lengkap</label>
                </div>
                <div class="inputbox">
                    <ion-icon name="person-outline"></ion-icon>
                    <input type="text" id="username">
                    <label for="">Username</label>
                </div>
                <div class="inputbox">
                    <ion-icon name="mail-outline"></ion-icon>
                    <input type="email" id="email">
                    <label for="">Email</label>
                </div>
                <div class="inputbox">
                    <ion-icon name="lock-closed-outline"></ion-icon>
                    <input type="password" id="password">
                    <label for="">Password</label>
                </div>
                <!-- <div class="forget">
                                        <label for=""><input type="checkbox">Remember Me</label> <a href="#">Forget Password</a>
                                    </div> -->
                <button id="register-btn">Register</button>
                <div class="register">
                    <p>Already have an account? Login!</p><a href="login">Login</a>
                </div>
            </div>
        </div>
    </section>
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
    <script>
        $(document).ready(function() {
            $("#register-btn").click(function() {
                var name = $("#name").val();
                var username = $("#username").val();
                var email = $("#email").val();
                var password = $("#password").val();

                if (name == "" || email == "" || username == "" || password == "") {
                    toastr.error("Semua kolom harus diisi !");
                } else {
                    $.ajax({
                        url: APIURL + "/v1/register",
                        method: "POST",
                        data: {
                            name: name,
                            email: email,
                            username: username,
                            password: password,
                        },
                        success: async function(response) {
                            //kalau berhasil register, akan masuk kesini
                            toastr.success("Berhasil Register!");
                            const statusSimpan = await updateSession(response.data);
                            if (statusSimpan == true) {
                                window.location.href = "/login";
                            }
                        },
                        error: function(error) {
                            //jika gagal register, akan masuk kesini
                            if (error.responseText)
                                var errorResponse = JSON.parse(error.responseText);
                            toastr.error(errorResponse.message);
                        }
                    });
                }
            });
        });
    </script>
@endsection