@extends('template')

@section('title', 'TopikChat - Edit Topik')

@section('content')
    <link href="{{ asset('css/edit-profile.css') }}" rel="stylesheet">

    <div class="container d-flex flex-column justify-content-center" style="height: 85vh">
        <h2 class="text-center mb-5">Edit Topik</h2>
        <div class="wrapp flex-column">
            <div class="card py-3 px-3">
                <div class="kolom mb-3">
                    <label for="judul_topik" class="form-label">Judul Topik</label>
                    <input type="text" id="judul_topik" name="judul_topik" class="form-control">
                </div>
                <div class="kolom mb-3 buled">
                    <label for="deskripsi" class="form-label">Deskripsi</label>
                    <textarea id="deskripsi" name="deskripsi" class="w-100 form-control"></textarea>
                </div>
                <div class="kolom mb-3 buled">
                    <label for="deskripsi" class="form-label">Pengaturan Privasi</label>
                    <select name="privasi" id="privasi" class="form select w-100">
                        <option value="0">Terbuka</option>
                        <option value="1">Tertutup</option>
                    </select>
                </div>
                <input type="submit" value="Edit Topik" class="form-control text-white buled"
                    style="background-color:#3DB18E;" id="edit-btn">
            </div>
        </div>
        <div id="loading" class="d-flex justify-content-center align-items-center"
            style="height: 100vh; background: rgba(0,0,0,0.5); position: fixed; top: 0; left: 0; right: 0; bottom: 0; z-index: 999; display: none;">
            <i class="fas fa-spinner fa-spin fa-3x"></i>
        </div>
    </div>

    <script>
        $(document).ready(async function() {
            var user = await getUser();
            var token = user.token;
            var topikId = "{{ $id }}";
            const judulInput = $("#judul_topik");
            const deskripsi = $("#deskripsi");
            const privasiInput = $("#privasi");
            const loading = $("#loading");

            function setupAjax() {
                $.ajaxSetup({
                    headers: {
                        "Authorization": "Bearer " + user['token'],
                    },
                });
            }

            function editTopikData() {
                $.ajax({
                    url: APIURL + "/v1/group-chat/" + topikId,
                    method: "GET",
                    success: function(response) {
                        console.log(response);
                        if (user['id'] == response.data.user_id) {
                            judulInput.val(response.data.group_name);
                            deskripsi.val(response.data.description);
                            privasiInput.val(response.data.is_private);
                            // window.location.href = "/v1/group-chat/" + topikId;
                        } else {
                            toastr.error("Anda bukan pemilik grup ini");
                            setTimeout(function() {
                                window.history.back();

                             }, 100);
                        }
                    },
                    error: async function(error) {
                        var response = error.responseJSON;
                        if (response.statuts == 401) {
                            toastr.error("Sesi habis, anda harus login kembali");
                            await deleteSession;
                            window.location.href = "/login";
                        } else {
                            toastr.error(response.message);
                        }
                    },
                    complete: async function(response) {
                        response = response.responseJSON;
                        if (response.newToken) {
                            user.token = response.newToken;
                            await updateSession(user); 
                            token = response.newToken;
                        }
                        loading.addClass("hidden");
                    }

                });
            }

            $("#edit-btn").click(async function() {
                loading.removeClass("hidden");
                $.ajax({
                    url: APIURL + "/v1/group-chat/" + topikId,
                    method: "PUT",
                    data: {
                        group_name: judulInput.val(),
                        description: deskripsi.val(),
                        is_private: privasiInput.val(),
                    },
                    success: async function(response) {
                        toastr.success("Berhasil mengubah data!");
                        setTimeout(function() {
                            window.location.href = "/group-chat/" + topikId;
                        }, 1000);
                    },
                    error: async function(error) {
                        var response = error.responseJSON;

                        //jika ada status 401 di response, maka dia harus delete token yg ada di sesi dan redirect ke login 
                        if (response.status == 401) {
                            toastr.error("Sesi habis, anda harus login kembali");
                            await deleteSession;
                            window.location.href = "/login";
                        } else {
                            toastr.error(response.message);
                        }
                    },

                    complete: async function() {
                        response = response.responseJSON;
                        if (response.newToken) {
                            user.token = response.newToken;
                            await updateSession(user);
                            token = response.newToken;
                        }
                        loading.addClass("hidden");
                    }
                });
            });

            setupAjax();
            editTopikData();
        });
    </script>
@endsection
