@extends("auth_template")

@section("title", "TopikChat - Login")

@section("content")
<div class="background">


    <div class="container py-3">
        <div class="d-flex justify-content-center">
            <img src="img/chat.png" alt="100"  width="180" height="180">
         </div>
    <div class="container py-3">
        <div class="container h-100">
            <div class="row h-50 d-flex justify-content-center align-items-center">
                    <div class="card">
                         <div class="card-body">
                                <div class="d-flex justify-content-center">
                                    <h1 class="form-tittle">TopikChat</h1>
                                </div>
                                <div class="d-flex justify-content-center">
                                    <h3>Login</h3>
                                </div>
                            <form>
                                <div class="mb-3">
                                    <label for="email" class="form-label">Email/Username</label>
                                    <input type="email" value="{{ old('email') }}" name="email" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label for="password" class="form-label">Password</label>
                                    <input type="password" name="password" class="form-control">
                                </div>
                                <div class="mb-3">   
                                    <input type="submit" value="login" class="form-control text-white selector" style="background-color:#3DB18E;">
                                </div>
                                <div class="mt-3 text-center py-2">
                                    <p>Belum Punya Akun?  <a href="/register"  style="color:#3DB18E;">Register</a></p>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
